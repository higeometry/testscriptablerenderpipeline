﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


#if UNITY_EDITOR
[InitializeOnLoad]
#endif
[System.Serializable]
public class TranslucentGameObjectManager {

     static TranslucentGameObjectManager s_instance;
     bool m_bInitialized;
     List<TranslucentGameObjectController> m_controllers = new List<TranslucentGameObjectController>();

    static TranslucentGameObjectManager()
    {
#if UNITY_EDITOR

        InitInEditor();
#endif  //UNITY_EDITOR
    }


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static private void InitInRuntime()
    {

#if !UNITY_EDITOR
        s_instance = new TranslucentGameObjectManager();
        Debug.Assert(m_bInitialized == false);
        m_bInitialized = true;
#endif
    }

    static private void InitInEditor()
    {
        s_instance = new TranslucentGameObjectManager();

        Debug.Log("TranslucentGameObjectManager created");
        s_instance.m_bInitialized = true;
    }

    static public bool AddIfNotRegistered(TranslucentGameObjectController go)
    {
        if (!s_instance.m_controllers.Contains(go))
        {
            s_instance.m_controllers.Add(go);
            return true;
        }
        return false;
    }

    static public bool RemoveIfRegistered(TranslucentGameObjectController go)
    {
        if (s_instance.m_controllers.Contains(go))
        {
        if (s_instance.m_controllers.Contains(go))
                s_instance.m_controllers.Remove(go);
            return true;
        }
        return false;
    }

    static public List<TranslucentGameObjectController> controllers
    {
        get
        {
            return s_instance.m_controllers;
        }

    }

}
