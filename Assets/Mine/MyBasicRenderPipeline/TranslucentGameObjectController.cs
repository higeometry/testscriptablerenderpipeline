﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class TranslucentGameObjectController : MonoBehaviour {
    public Vector4 m_AfterRotTransPers;
#if UNITY_EDITOR
    // User codes are sometimes compiled in Editor.
    // but Awake() or Start() won't be called after that compilation.
    // In such cases, the content of some singletons would become empty.
    // to avoid this situation, registering controllers in MonoBehaviour constructors is useful.
    private TranslucentGameObjectController()
    {
        TranslucentGameObjectManager.AddIfNotRegistered(this);
    }
#endif
    void Awake()
    {
        TranslucentGameObjectManager.AddIfNotRegistered(this);
        // Debug.Log("Awake");

    }

    void Start () {
    }
	
	void Update () {
		
	}

    void OnDestroy()
    {
        TranslucentGameObjectManager.RemoveIfRegistered(this);
        // Debug.Log("Destroyed");
    }
}
