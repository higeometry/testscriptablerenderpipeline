//#define USE_COMMANDBUFFER 

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;
using UnityEngine.XR;

// Very basic scriptable rendering loop example:
// - Use with BasicRenderPipelineShader.shader (the loop expects "BasicPass" pass type to exist)
// - Supports up to 8 enabled lights in the scene (directional, point or spot)
// - Does the same physically based BRDF as the Standard shader
// - No shadows
// - This loop also does not setup lightmaps, light probes, reflection probes or light cookies

[ExecuteInEditMode]
public class FwdBasicRenderPipeline : RenderPipelineAsset
{
    public bool UseIntermediateRenderTargetBlit;
    public bool UseZPrepass;
#if UNITY_EDITOR
    [UnityEditor.MenuItem("RenderPipeline/Create FwdBasicRenderPipeline")]
    static void CreateFwdBasicRenderPipeline()
    {
        var instance = ScriptableObject.CreateInstance<FwdBasicRenderPipeline>();
        UnityEditor.AssetDatabase.CreateAsset(instance, "Assets/Mine/UnityChan/FwdBasicRenderPipeline.asset");
    }

#endif

    protected override IRenderPipeline InternalCreatePipeline()
    {
        return new FwdBasicRenderPipelineInstance(UseIntermediateRenderTargetBlit, UseZPrepass);
    }
}

public class FwdBasicRenderPipelineInstance : RenderPipeline
{
    bool useIntermediateBlit;
    bool useZPrepass;
    public FwdBasicRenderPipelineInstance()
    {
        useIntermediateBlit = false;
    }

    public FwdBasicRenderPipelineInstance(bool useIntermediate, bool UseZPrepass)
    {
        useIntermediateBlit = useIntermediate;
        useZPrepass = UseZPrepass;
    }

    public override void Render(ScriptableRenderContext renderContext, Camera[] cameras)
    {
        base.Render(renderContext, cameras);
        FwdBasicRendering.Render(renderContext, cameras, useIntermediateBlit,useZPrepass);
    }
}

public static class FwdBasicRendering
{
    class LayerUpdateData
    {
        internal LayerUpdateData()
        {
            layeredGameObjects = new List<GameObject>();
        }
        internal List<GameObject> layeredGameObjects;
        internal Vector4 m_AfterRotTransPers;
    }

    static List<LayerUpdateData> m_layeredGameObjectsList = new List<LayerUpdateData>();
    static void ConfigureAndBindIntermediateRenderTarget(ScriptableRenderContext context, Camera cam, bool stereoEnabled, out RenderTargetIdentifier intermediateRTID, out bool isRTTexArray)
    {
        var intermediateRT = Shader.PropertyToID("_IntermediateTarget");
        intermediateRTID = new RenderTargetIdentifier(intermediateRT);

        isRTTexArray = false;

        var bindIntermediateRTCmd = CommandBufferPool.Get("Bind intermediate RT");

        if (stereoEnabled)
        {
            RenderTextureDescriptor xrDesc = XRSettings.eyeTextureDesc;
            xrDesc.depthBufferBits = 24;

            if (xrDesc.dimension == TextureDimension.Tex2DArray)
                isRTTexArray = true;

            bindIntermediateRTCmd.GetTemporaryRT(intermediateRT, xrDesc, FilterMode.Point);
        }
        else
        {
            int w = cam.pixelWidth;
            int h = cam.pixelHeight;
            bindIntermediateRTCmd.GetTemporaryRT(intermediateRT, w, h, 24, FilterMode.Point, RenderTextureFormat.Default, RenderTextureReadWrite.Default, 1, true);
        }

        if (isRTTexArray)
            bindIntermediateRTCmd.SetRenderTarget(intermediateRTID, 0, CubemapFace.Unknown, -1); // depthSlice == -1 => bind all slices
        else
            bindIntermediateRTCmd.SetRenderTarget(intermediateRTID);

        context.ExecuteCommandBuffer(bindIntermediateRTCmd);
        CommandBufferPool.Release(bindIntermediateRTCmd);
    }

    static void BlitFromIntermediateToCameraTarget(ScriptableRenderContext context, RenderTargetIdentifier intermediateRTID, bool isRTTexArray)
    {
        var blitIntermediateRTCmd = CommandBufferPool.Get("Copy intermediate RT to default RT");

        if (isRTTexArray)
        {
            // Currently, Blit does not allow specification of a slice in a texture array.
            // It can use the CurrentActive render texture's bound slices, so we use that
            // as a temporary workaround.
            blitIntermediateRTCmd.SetRenderTarget(BuiltinRenderTextureType.CameraTarget, 0, CubemapFace.Unknown, -1);
            blitIntermediateRTCmd.Blit(intermediateRTID, BuiltinRenderTextureType.CurrentActive);
        }
        else
            blitIntermediateRTCmd.Blit(intermediateRTID, BuiltinRenderTextureType.CameraTarget);

        context.ExecuteCommandBuffer(blitIntermediateRTCmd);
        CommandBufferPool.Release(blitIntermediateRTCmd);

    }
    
    // Main entry point for our scriptable render loop

    public static void Render(ScriptableRenderContext context, IEnumerable<Camera> cameras, bool useIntermediateBlitPath, bool useZPrepass)
    {
        bool stereoEnabled = XRSettings.isDeviceActive;
        UpdateLayerFlag();

        foreach (var camera in cameras)
        {
            // Culling
            ScriptableCullingParameters cullingParams;

            // Stereo-aware culling parameters are configured to perform a single cull for both eyes
            if (!CullResults.GetCullingParameters(camera, stereoEnabled, out cullingParams))
                continue;
            cullingParams.cullingMask =  0x3fffffff;
            CullResults cull = new CullResults();
            CullResults.Cull(ref cullingParams, context, ref cull);

            // Setup camera for rendering (sets render target, view/projection matrices and other
            // per-camera built-in shader variables).
            // If stereo is enabled, we also configure stereo matrices, viewports, and XR device render targets
            context.SetupCameraProperties(camera, stereoEnabled);

            // Draws in-between [Start|Stop]MultiEye are stereo-ized by engine
            if (stereoEnabled)
                context.StartMultiEye(camera);

            RenderTargetIdentifier intermediateRTID = new RenderTargetIdentifier(BuiltinRenderTextureType.CurrentActive);
            bool isIntermediateRTTexArray = false;
            if (useIntermediateBlitPath)
            {
                ConfigureAndBindIntermediateRenderTarget(context, camera, stereoEnabled, out intermediateRTID, out isIntermediateRTTexArray);
            }

            // clear depth buffer
            var cmd = CommandBufferPool.Get();
            cmd.ClearRenderTarget(true, false, Color.black);
            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);

            // Setup global lighting shader variables
            SetupLightShaderVariables(cull.visibleLights, context);

            // Draw opaque objects using BasicPass shader pass
            var drawSettings = new DrawRendererSettings(camera, new ShaderPassName("ForwardBase")) { sorting = { flags = SortFlags.CommonOpaque } };
            var filterSettings = new FilterRenderersSettings(true) { renderQueueRange = RenderQueueRange.opaque };
             context.DrawRenderers(cull.visibleRenderers, ref drawSettings, filterSettings);

            // Draw skybox
            context.DrawSkybox(camera);

            // Custom pass. z prepass 
            ScriptableCullingParameters cullingParams2;
            // Stereo-aware culling parameters are configured to perform a single cull for both eyes
            if (CullResults.GetCullingParameters(camera, stereoEnabled, out cullingParams2))
            {
#if !USE_COMMANDBUFFER
                m_layeredGameObjectsList = Zsort(camera.worldToCameraMatrix, m_layeredGameObjectsList);

                foreach (var list in  m_layeredGameObjectsList )
                {
                    
                    foreach(var go in list.layeredGameObjects)
                    {
                        go.layer = 31;
                    }

                    cullingParams2.cullingMask = (1 << 31);
                    CullResults cull3 = new CullResults();
                    CullResults.Cull(ref cullingParams2, context, ref cull3);
                    var drawSettings3 = new DrawRendererSettings(camera, new ShaderPassName("ZPrepass")) { sorting = { flags = SortFlags.CommonTransparent } };
                    var filterSettings3 = new FilterRenderersSettings(true) { renderQueueRange = RenderQueueRange.transparent };
                    if (useZPrepass)
                        context.DrawRenderers(cull3.visibleRenderers, ref drawSettings3, filterSettings3);
                    var drawSettings4 = new DrawRendererSettings(camera, new ShaderPassName("ForwardBase")) { sorting = { flags = SortFlags.CommonTransparent } };
                    context.DrawRenderers(cull3.visibleRenderers, ref drawSettings4, filterSettings3);

                    
                    foreach (var go in list.layeredGameObjects)
                    {
                        go.layer = 1;
                    }
                    
                }


#else
                var cmdBuffer = new CommandBuffer();
                m_layeredGameObjectsList = Zsort(camera.worldToCameraMatrix, m_layeredGameObjectsList);
                foreach ( var list in m_layeredGameObjectsList)
                {
                    DrawGameObjectTranslucent(cmdBuffer, list, 0);
                    DrawGameObjectTranslucent(cmdBuffer, list, 1);
                }
                context.ExecuteCommandBuffer(cmdBuffer);
                CommandBufferPool.Release(cmdBuffer);

#endif


                }


            // Draw transparent objects using BasicPass shader pass
            drawSettings.sorting.flags = SortFlags.CommonTransparent;
            filterSettings.renderQueueRange = RenderQueueRange.transparent;
            context.DrawRenderers(cull.visibleRenderers, ref drawSettings, filterSettings);

            if (useIntermediateBlitPath)
            {
                BlitFromIntermediateToCameraTarget(context, intermediateRTID, isIntermediateRTTexArray);
            }

            if (stereoEnabled)
            {
                context.StopMultiEye(camera);
                // StereoEndRender will reset state on the camera to pre-Stereo settings,
                // and invoke XR based events/callbacks.
                context.StereoEndRender(camera);
            }

            context.Submit();
            RestoreLayerFlag();
        }
    }

    private static void UpdateLayerFlag()
    {
        m_layeredGameObjectsList.Clear();
        var controllers = TranslucentGameObjectManager.controllers;
        foreach (var controller in controllers)
        {
            var layerUpdateData = new LayerUpdateData();
            m_layeredGameObjectsList.Add(layerUpdateData);
            UpdateLayerFlagSub(controller.gameObject, layerUpdateData);
        }

    }

    private static void UpdateLayerFlagSub(GameObject go, LayerUpdateData layerUpdateData)
    {
        //TranslucentGameObjectController controller
        if ( go.GetComponent<MeshRenderer>() != null || go.GetComponent<SkinnedMeshRenderer>() != null )
        {
#if USE_COMMANDBUFFER
            go.layer =  31;
#else
            go.layer = 30;
#endif
            layerUpdateData.layeredGameObjects.Add(go);
        }

        var transform = go.transform;
        int count = transform.childCount;
        for (int ii = 0; ii < count; ii++)
        {
            var childTrans = transform.GetChild(ii);
            UpdateLayerFlagSub(childTrans.gameObject, layerUpdateData);
        }
    }

    private static void RestoreLayerFlag()
    {
        foreach (var list in m_layeredGameObjectsList)
        {
            foreach (GameObject go in list.layeredGameObjects)
            {
                go.layer = 0;
            }
        }
    }
    private static void DrawGameObjectTranslucent(CommandBuffer cmdBuffer, LayerUpdateData layerUpdateData, int index)
    {
        string[] str =
        {
                "ZPREPASS",
                "FORWARD", // "ForwardBase"
        };

        foreach ( GameObject go in layerUpdateData.layeredGameObjects)
        {
            Renderer renderer = go.GetComponent<MeshRenderer>();
            if (renderer == null)
            {
                renderer = go.GetComponent<SkinnedMeshRenderer>();
            }
            Material[] materials = null;
            if (renderer != null)
            {
                materials = renderer.sharedMaterials;
            }

            if (go.active == false)
            {
                continue;
            }
            int passIndex = -1;
            if (materials != null)
            {
                for (int materialIndex = 0; materialIndex < materials.Length; materialIndex++)
                {
                    var material = materials[materialIndex];
                    passIndex = material.FindPass(str[index]);
                    if (passIndex != -1)
                    {
                        cmdBuffer.DrawRenderer(renderer, material, materialIndex, passIndex);
                    }
                }
            }

        }
    }
    private static void DrawGameObjectTranslucent(CommandBuffer cmdBuffer, GameObject go,int index)
    {
        string[] str =
        {
                "ZPREPASS",
                "FORWARD", // "ForwardBase"
        };

        Renderer renderer = go.GetComponent<MeshRenderer>();
        if (renderer == null)
        {
            renderer = go.GetComponent<SkinnedMeshRenderer>();
        }
        Material[] materials = null;
        if (renderer != null)
        {
            materials = renderer.sharedMaterials;
        }
        

        int passIndex = -1;
        if (materials != null)
        {
            for ( int materialIndex = 0; materialIndex < materials.Length; materialIndex++)
            {
                var material = materials[materialIndex];
                passIndex = material.FindPass(str[index]);
                if (passIndex != -1)
                {
                    cmdBuffer.DrawRenderer(renderer, material, materialIndex, passIndex);
                }
            }
        }

        for (int ii = 0; ii < go.transform.childCount; ii++)
        {

            var child = go.transform.GetChild(ii).gameObject;
            DrawGameObjectTranslucent(cmdBuffer, child, index);
 

        }
    }

    private static List<LayerUpdateData> Zsort(Matrix4x4 mat, List<LayerUpdateData> layeredGameObjectsList)
    {
        foreach (var item in layeredGameObjectsList)
        {
            var transform = item.layeredGameObjects[0].transform.position;
            Vector4 v = new Vector4(transform.x, transform.y, transform.z);
            v = mat * v;
            item.m_AfterRotTransPers = v;

        }
        layeredGameObjectsList.Sort(SortyByZ2);
        return layeredGameObjectsList;
    }

    static int SortyByZ2(LayerUpdateData p1, LayerUpdateData p2)
    {
       return p1.m_AfterRotTransPers.z.CompareTo(p2.m_AfterRotTransPers.z);

    }
private static List<TranslucentGameObjectController>  Zsort(Matrix4x4 mat, List<TranslucentGameObjectController> goList)
    {
        foreach (var item in goList)
        {
            var transform = item.gameObject.transform.position;
            Vector4 v = new Vector4(transform.x, transform.y, transform.z);
            v = mat * v;
            item.m_AfterRotTransPers = v;
        }
        goList.Sort(SortByZ);
        return goList;
    }

    
    static int SortByZ(TranslucentGameObjectController p1, TranslucentGameObjectController p2)
    {
        return p1.m_AfterRotTransPers.z.CompareTo(p2.m_AfterRotTransPers.z);
    }

    // Setup lighting variables for shader to use

    private static void SetupLightShaderVariables(List<VisibleLight> lights, ScriptableRenderContext context)
    {
        // We only support up to 8 visible lights here. More complex approaches would
        // be doing some sort of per-object light setups, but here we go for simplest possible
        // approach.
        const int kMaxLights = 8;
        // Just take first 8 lights. Possible improvements: sort lights by intensity or distance
        // to the viewer, so that "most important" lights in the scene are picked, and not the 8
        // that happened to be first.
        int lightCount = Mathf.Min(lights.Count, kMaxLights);

        // Prepare light data
        Vector4[] lightColors = new Vector4[kMaxLights];
        Vector4[] lightPositions = new Vector4[kMaxLights];
        Vector4[] lightSpotDirections = new Vector4[kMaxLights];
        Vector4[] lightAtten = new Vector4[kMaxLights];
        for (var i = 0; i < lightCount; ++i)
        {
            VisibleLight light = lights[i];
            lightColors[i] = light.finalColor;
            if (light.lightType == LightType.Directional)
            {
                // light position for directional lights is: (-direction, 0)
                var dir = light.localToWorld.GetColumn(2);
                lightPositions[i] = new Vector4(-dir.x, -dir.y, -dir.z, 0);
            }
            else
            {
                // light position for point/spot lights is: (position, 1)
                var pos = light.localToWorld.GetColumn(3);
                lightPositions[i] = new Vector4(pos.x, pos.y, pos.z, 1);
            }
            // attenuation set in a way where distance attenuation can be computed:
            //  float lengthSq = dot(toLight, toLight);
            //  float atten = 1.0 / (1.0 + lengthSq * LightAtten[i].z);
            // and spot cone attenuation:
            //  float rho = max (0, dot(normalize(toLight), SpotDirection[i].xyz));
            //  float spotAtt = (rho - LightAtten[i].x) * LightAtten[i].y;
            //  spotAtt = saturate(spotAtt);
            // and the above works for all light types, i.e. spot light code works out
            // to correct math for point & directional lights as well.

            float rangeSq = light.range * light.range;

            float quadAtten = (light.lightType == LightType.Directional) ? 0.0f : 25.0f / rangeSq;

            // spot direction & attenuation
            if (light.lightType == LightType.Spot)
            {
                var dir = light.localToWorld.GetColumn(2);
                lightSpotDirections[i] = new Vector4(-dir.x, -dir.y, -dir.z, 0);

                float radAngle = Mathf.Deg2Rad * light.spotAngle;
                float cosTheta = Mathf.Cos(radAngle * 0.25f);
                float cosPhi = Mathf.Cos(radAngle * 0.5f);
                float cosDiff = cosTheta - cosPhi;
                lightAtten[i] = new Vector4(cosPhi, (cosDiff != 0.0f) ? 1.0f / cosDiff : 1.0f, quadAtten, rangeSq);
            }
            else
            {
                // non-spot light
                lightSpotDirections[i] = new Vector4(0, 0, 1, 0);
                lightAtten[i] = new Vector4(-1, 1, quadAtten, rangeSq);
            }
        }

        // ambient lighting spherical harmonics values
        const int kSHCoefficients = 7;
        Vector4[] shConstants = new Vector4[kSHCoefficients];
        SphericalHarmonicsL2 ambientSH = RenderSettings.ambientProbe * RenderSettings.ambientIntensity;
        GetShaderConstantsFromNormalizedSH(ref ambientSH, shConstants);

        // setup global shader variables to contain all the data computed above
        CommandBuffer cmd = CommandBufferPool.Get();
        cmd.SetGlobalVectorArray("globalLightColor", lightColors);
        cmd.SetGlobalVectorArray("globalLightPos", lightPositions);
        cmd.SetGlobalVectorArray("globalLightSpotDir", lightSpotDirections);
        cmd.SetGlobalVectorArray("globalLightAtten", lightAtten);
        cmd.SetGlobalVector("globalLightCount", new Vector4(lightCount, 0, 0, 0));
        cmd.SetGlobalVectorArray("globalSH", shConstants);
        context.ExecuteCommandBuffer(cmd);
        CommandBufferPool.Release(cmd);
    }

    // Prepare L2 spherical harmonics values for efficient evaluation in a shader

    private static void GetShaderConstantsFromNormalizedSH(ref SphericalHarmonicsL2 ambientProbe, Vector4[] outCoefficients)
    {
        for (int channelIdx = 0; channelIdx < 3; ++channelIdx)
        {
            // Constant + Linear
            // In the shader we multiply the normal is not swizzled, so it's normal.xyz.
            // Swizzle the coefficients to be in { x, y, z, DC } order.
            outCoefficients[channelIdx].x = ambientProbe[channelIdx, 3];
            outCoefficients[channelIdx].y = ambientProbe[channelIdx, 1];
            outCoefficients[channelIdx].z = ambientProbe[channelIdx, 2];
            outCoefficients[channelIdx].w = ambientProbe[channelIdx, 0] - ambientProbe[channelIdx, 6];
            // Quadratic polynomials
            outCoefficients[channelIdx + 3].x = ambientProbe[channelIdx, 4];
            outCoefficients[channelIdx + 3].y = ambientProbe[channelIdx, 5];
            outCoefficients[channelIdx + 3].z = ambientProbe[channelIdx, 6] * 3.0f;
            outCoefficients[channelIdx + 3].w = ambientProbe[channelIdx, 7];
        }
        // Final quadratic polynomial
        outCoefficients[6].x = ambientProbe[0, 8];
        outCoefficients[6].y = ambientProbe[1, 8];
        outCoefficients[6].z = ambientProbe[2, 8];
        outCoefficients[6].w = 1.0f;
    }
}
